from annoy import AnnoyIndex
from parser import Parser
from database import Database


class Recommender:

    def __init__(self, n, user_id):
        self.n = n
        self.user_id = user_id

    def get_recommended_stud_clubs(self, parser=Parser()):
        connection = Database().get_connection()
        with connection.cursor() as cursor:
            users_interests = parser.get_topics_by_user_id(self.user_id)
            if len(users_interests) == 0:
                cursor.execute("SELECT id FROM communities WHERE link_vk IS NOT NULL ORDER BY RAND() LIMIT 10")
                community_ids = list(map(lambda x: x[0], list(cursor)))
            else:
                cursor.execute("SELECT COUNT(DISTINCT(topic_id)) FROM community_topic")
                dimensions = cursor.fetchone()[0]
                # model = AnnoyIndex(dimensions, 'angular')
                model = AnnoyIndex(dimensions, 'manhattan')
                cursor.execute("SELECT community_id, relative_frequency FROM community_topic")
                all_freq = cursor.fetchall()
                frequencies = []
                for i in range(len(all_freq)):
                    frequencies.append(all_freq[i][1])
                    if len(frequencies) == dimensions:
                        model.add_item(all_freq[i][0], frequencies)
                        frequencies.clear()
                model.build(self.n)
                community_ids = model.get_nns_by_vector(users_interests, self.n)
            recommended_communities = []
            for id in community_ids:
                cursor.execute("SELECT name, link_vk, description, photo_id FROM communities where id = {}".format(id))
                recommended_communities.append(cursor.fetchone())
            return recommended_communities
