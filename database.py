import pymysql
import variables


class Database:

    def get_connection(self):
        connection = pymysql.connect(host=variables.db_host, port=variables.db_port, user=variables.db_username,
                                     password=variables.db_password, db=variables.db_name)
        return connection

    def get_by_theme_id(self, id):
        connection = self.get_connection()
        with connection.cursor() as cursor:
            sql = "SELECT communities.name, communities.link_vk, communities.description, communities.photo_id, themes.name FROM communities JOIN themes ON theme_id = themes.id WHERE themes.id = %s "
            cursor.execute(sql, id)
            line = cursor.fetchall()
            return line

    def get_themes(self):
        connection = self.get_connection()
        with connection.cursor() as cursor:
            sql = "SELECT id, name FROM themes"
            cursor.execute(sql)
            line = cursor.fetchall()
            return line

    def add_mailing(self, id: int):
        connection = self.get_connection()
        with connection.cursor() as cursor:
            sql = f"INSERT INTO mailing(`user_id`) VALUES({id}) ON DUPLICATE KEY UPDATE `user_id` = {id}"
            cursor.execute(sql)
            connection.commit()

    def get_mailing_ids(self):
        connection = self.get_connection()
        with connection.cursor() as cursor:
            sql = "SELECT user_id FROM mailing"
            cursor.execute(sql)
            line = cursor.fetchall()
            return line

# studclubs = {1 : sno_inchem, 2:student_science,4:scientists_ifk, 5:fractal, 6:asmi,7:snoinbio,8: sci_gum,11:accent,12:kvadrat,13:projam,14:pro_sebya,16:myata,18:juice,20:nota_bene,21:tandem,22:poslushayte,23:tramticket,24:green_light,25:genzakk,26: drug_drugu,27: alivepoetsociety,28: oku_craft,29: sigma,31: colorit,32: continent,35: phula,36: lapki,40: ligadobrovoltshev,41: white_cane,42: fiveR,44: phoenix,46: delta,48: green_vector,49: goodness,50: pravovye,51: sunflower,52: utmn_media,53: fei_media,54: ion,55: efir_media,56: geolocation,57: inbio,58: ifkmedia,59: light_media,60: utmn_so , 62: strela,63: rassvet,64: finist,65: fortuna,67: pikchernaya,68: makerthon,69: finclub,71: business_club,72: spe,73: ctf,74: soboli,75: cybersport,76: ladya,77: hockey,78: cybersports_imikn,79: edinoborstva,80: chess,84: poisk_utmn,85: barsy,86: utmn_zapobedu,87: aisutmn, 88:aiesec_tyumen ,89:geekgalaxy,90: teaplizza,91: esenin,92: kvartirnik,93: soviet,94: oon,95: spekclub,96: daria_doncova,97: babel_tower,98: intellekt_iq,99: utmn_se, 100: welcome_utmn,101: turkclub }

if __name__ == "__main__":
    database = Database()
    for i in range(len(new)):
        sql = "INSERT INTO community_topic (community_id, topic_id, relative_frequency) VALUES(%s, %s, %s)"
        with database.get_connection() as connection:
            with connection.cursor() as cursor:
                cursor.execute(sql, (104, i + 1, new[i]))
                connection.commit()
