import requests
import numpy as np
import vk_api
from bs4 import BeautifulSoup
import json
from variables import token

np.set_printoptions(suppress=True)
vk_session = vk_api.VkApi(token=token)
Not_needed = ('Открытая группа', 'Закрытая группа', 'Публичная страница')


class Parser:
    def __init__(self):
        self.url = 'https://www.utmn.ru/studentam/molodezhka/stud-soobshchestva/'
        self.basenames = ['nauka', 'tvorchestvo/khoreografiya', 'tvorchestvo/muzyka', 'tvorchestvo/teatr-i-literatura',
                          'tvorchestvo/moda-i-dizayn', 'tvorchestvo/original', 'tvorchestvo/fotoiskusstvo',
                          'tvorchestvo/kvn', 'volontery', 'media', 'professional',
                          'sport-i-zozh',
                          'patriotic', 'international', 'communication', 'intellekt', 'sluzhba-etiketa', 'turizm']
        self.topics_user_subs_freq = json.load(open("topics.json", "r", encoding="utf-8"))

    def get_stats_community(self, id):
        community_info = vk_session.method("stats.get", {"group_id": id})
        return community_info

    def get_topics_by_user_id(self, user_id):
        user_info = vk_session.method("users.get", {"user_id": user_id})
        if user_info[0]['is_closed']:
            return []
        else:
            topics_user_subs_freq = self.topics_user_subs_freq
            for k in topics_user_subs_freq.keys():
                topics_user_subs_freq[k] = 0
            groups = vk_session.method("groups.get", {
                "user_id": user_id,
                "extended": 1,
                "type": "page",
                "filter": "publics",
                "fields": "activity"
            })
            topics = []
            for group in groups['items']:
                if 'activity' in group:
                    if group['activity'][:4] != 'Этот' and group['activity'][:6] != 'Данный' and group['activity'] not in Not_needed:
                        topics.append(group['activity'])
            for topic in topics:
                if topic in list(topics_user_subs_freq.keys()):
                    topics_user_subs_freq[topic] += 1
            freq_list = list(topics_user_subs_freq.values())
            if sum(freq_list) == 0:
                return []
            # return freq_list
            freq_array = np.array(freq_list)
            return list(freq_array / sum(freq_array))

    def build_url(self, access_token, users_ids):
        if type(users_ids) == int:
            api = 'API.groups.get({{\'user_id\': {}, \'extended\': 1, \'filter\': \'publics\', \'fields\': \'activity,type\'}})'.format(
                users_ids)
            url = 'https://api.vk.com/method/execute?access_token={}&v=5.131&code=return%20[{}];'.format(
                access_token, api)
            return url
        if type(users_ids) == list:
            users_ids_n = len(users_ids)

            if users_ids_n > 0 and users_ids_n < 26:
                api = 'API.groups.get({{\'user_id\': {}, \'extended\': 1, \'filter\': \'publics\', \'fields\': \'activity,type\'}})'.format(
                    users_ids[0])
                for i in range(1, users_ids_n):
                    api += ',API.groups.get({{\'user_id\':{},\'extended\':1, \'filter\': \'publics\', \'fields\' :\'activity,type\'}})'.format(
                        users_ids[i])
                url = 'https://api.vk.com/method/execute?access_token={}&v=5.131&code=return%20[{}];'.format(
                    access_token, api)
                return url
        else:
            return "No users_ids"

    def count_topics_freq_group_less_25(self, group_id):
        topics_user_subs_freq = self.topics_user_subs_freq

        for k in topics_user_subs_freq:
            topics_user_subs_freq[k] = 0
        group_info = vk_session.method("groups.getMembers", {"group_id": group_id})
        max = 25

        len_subscribers = len(group_info['items'])
        if len_subscribers <= max:
            groups = self.build_url(token, group_info['items'][::])
            response = requests.get(groups).json()['response']
            len_subscribers_not_closed = len_subscribers - response.count(False)
            for i in range(len(response)):
                if response[i]:
                    items = response[i]['items']
                    for item in items:
                        if 'activity' in item:
                            if item['activity'] not in Not_needed and item['activity'][:4] != 'Этот':
                                topics_user_subs_freq[item['activity']] += 1

            for k in topics_user_subs_freq:
                topics_user_subs_freq[k] /= len_subscribers_not_closed
        # return list(topics_user_subs_freq.values())
        freq_array = np.array(list(topics_user_subs_freq.values()))
        return list(freq_array / np.sum(freq_array))


    def count_topics_freq_group_more_25(self, group_id):
        topics_user_subs_freq = self.topics_user_subs_freq
        for k in topics_user_subs_freq:
            topics_user_subs_freq[k] = 0
        group_info = vk_session.method("groups.getMembers", {"group_id": group_id})
        if group_info['count'] > 0:

            len_subscribers = group_info['count']
            if len_subscribers > 1000:
                offset = 1000
                while offset < len_subscribers:
                    group_info['items'].extend(
                        vk_session.method("groups.getMembers", {"group_id": group_id, "offset": offset})['items'])
                    offset += 1000
            len_subscribers_not_closed = len_subscribers
            responses = []
            max = 25
            for idx in range(1, len_subscribers // max + 1):
                if len_subscribers - idx * max >= max:
                    groups = self.build_url(token, group_info['items'][(idx - 1) * max:max * idx])
                    response = requests.get(groups).json()['response']
                    len_subscribers_not_closed -= response.count(False)
                    for i in range(len(response)):
                        if response[i]:
                            items = response[i]['items']
                            for item in items:
                                if 'activity' in item:
                                    if item['activity'] not in Not_needed and item['activity'][:4] != 'Этот' and item[
                                        'activity'] in topics_user_subs_freq.keys():
                                        topics_user_subs_freq[item['activity']] += 1
                else:
                    groups = self.build_url(token, group_info['items'][(idx * max):len_subscribers])
                    response = requests.get(groups).json()['response']
                    len_subscribers_not_closed -= response.count(False)
                    for i in range(len(response)):
                        if response[i]:
                            items = response[i]['items']
                            for item in items:
                                if 'activity' in item:
                                    if item['activity'] not in Not_needed and item['activity'][:4] != 'Этот':
                                        topics_user_subs_freq[item['activity']] += 1

            for k in topics_user_subs_freq:
                topics_user_subs_freq[k] /= len_subscribers_not_closed
            print(len_subscribers_not_closed)
            # return list(topics_user_subs_freq.values())
            freq_array = np.array(list(topics_user_subs_freq.values()))
            return list(freq_array/np.sum(freq_array))
        else:
            return 'Group is closed!'

    def get_clubs_urls(self):
        clubs_urls = []
        r = requests.get(url=self.url,
                         headers={"User-Agent": "Mozilla/5.0"})
        soup = BeautifulSoup(r.text, features='html.parser')
        for category in soup.find_all('h2'):
            url_category = category.find('a').get('href')
            r = requests.get(url=url_category,
                             headers={
                                 "User-Agent": "Mozilla/5.0"})
            soup = BeautifulSoup(r.text, features='html.parser')
            for elem in soup.find_all(class_=['b-study-category', 'static-layout__h1', 'effect-julia']):
                url = elem.find('a')
                if url:
                    url_club = url['href']
                    if 'https://www.utmn.ru/' not in url_club:
                        url_club = 'https://www.utmn.ru' + url_club
                    if "tvorchestvo" not in url_club and "samoupravlenie" not in url_club:
                        clubs_urls.append(url_club)
                    if "tvorchestvo" in url_club:
                        r = requests.get(url=url_club,
                                         headers={
                                             "User-Agent": "Mozilla/5.0"})
                        soup = BeautifulSoup(r.text, features='html.parser')
                        for club in soup.find_all(class_='b-study-category'):
                            url_club = club.find('a').get('href')
                            clubs_urls.append(url_club)
        return clubs_urls

    def get_clubs_images(self):
        images_urls = []
        for basename in self.basenames:
            r = requests.get(url=self.url + basename,
                             headers={
                                 "User-Agent": "Mozilla/5.0"})
            soup = BeautifulSoup(r.text, features='lxml')
            images = soup.find_all('span', class_="study-cat-pic")
            for span in images:
                img = span.find('img')
                image_url = "https://www.utmn.ru" + img.get('src')
                images_urls.append((image_url))
        return images_urls
