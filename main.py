import math
import time
import json
from vkbottle.bot import Message, MessageEvent
from vkbottle import Bot, KeyboardButtonColor, EMPTY_KEYBOARD, template_gen, GroupEventType, GroupTypes, \
    VKAPIError, Keyboard, Callback, Text, \
    TemplateElement, ABCAction, OpenLink, ABCRule
from vkbottle.dispatch.rules.base import RegexRule
from variables import token_group
from recommender import Recommender
from database import Database


class Permission(ABCRule[Message]):
    def __init__(self, user_ids):
        if not isinstance(user_ids, list):
            user_ids = [user_ids]
        self.uids = user_ids

    async def check(self, event: Message):
        return event.from_id in self.uids


user_info = {}
database = Database()
bot = Bot(token=token_group)
bot.labeler.vbml_ignore_case = True
bot.labeler.custom_rules["permission"] = Permission


@bot.on.message(text=["начать", "start"])
async def handler(message: Message):
    database.add_mailing(message.from_id)
    # keyboard_choice = (
    #     Keyboard().add(Text("Что есть на UTMN FEST"),
    #                    color=KeyboardButtonColor.NEGATIVE
    #                    ).row()
    #     .add(Text("Начать подбор объединений"),
    #          color=KeyboardButtonColor.PRIMARY
    #          ).row()
    #     .add(Text("Показать все объединения"))
    #     .add(Text("Задать вопрос организаторам"))
    #     .get_json()
    # )
    # intro_message = "👋🏻 Привет! Мы рады приветствовать тебя на UTMN FEST!\n\n" \
    #                 "✅ Чтобы узнать, какие есть активности на UTMN FEST, " \
    #                 "нажми на кнопку «Что есть на UTMN FEST» или отправь сообщение «FEST»\n" \
    #                 "✅ Чтобы посмотреть подборку студенческих объединений ТюмГУ, которые могут тебя заинтересовать, " \
    #                 "нажми на кнопку «Начать подбор объединений» или напиши «Подбор»\n" \
    #                 "✅ Чтобы посмотреть все студенческие объединения ТюмГУ, " \
    #                 "нажми на кнопку «Показать все объединения» или отправь сообщение «Все»\n" \
    #                 "✅ Чтобы задать вопрос организаторам UTMN FEST, " \
    #                 "нажми на кнопку «Задать вопрос организаторам» или напиши «Вопрос»\n\n" \
    #                 "Продолжая работу с ботом, ты разрешаешь обработку своих персональных данных и получение сообщений. " \
    #                 "Организатор UTMN FEST – @molpolutmn (Молодёжка ТюмГУ)."
    keyboard_choice = (
        Keyboard().add(Text("Начать подбор объединений"),
                 color=KeyboardButtonColor.PRIMARY
                 ).row()
            .add(Text("Показать все объединения"))
            .add(Text("Задать вопрос организаторам"))
            .get_json()
    )
    intro_message = "👋🏻 Привет! Мы рады приветствовать тебя на UTMN FEST!\n\n" \
                    "✅ Чтобы посмотреть подборку студенческих объединений ТюмГУ, которые могут тебя заинтересовать, " \
                    "нажми на кнопку «Начать подбор объединений» или напиши «Подбор»\n" \
                    "✅ Чтобы посмотреть все студенческие объединения ТюмГУ, " \
                    "нажми на кнопку «Показать все объединения» или отправь сообщение «Все»\n" \
                    "✅ Чтобы задать вопрос организаторам UTMN FEST, " \
                    "нажми на кнопку «Задать вопрос организаторам» или напиши «Вопрос»\n\n" \
                    "Продолжая работу с ботом, ты разрешаешь обработку своих персональных данных и получение сообщений. " \
                    "Организатор UTMN FEST – @molpolutmn (Молодёжка ТюмГУ)."
    await message.answer(
        message=intro_message,
        keyboard=keyboard_choice, random_id=0)


@bot.on.message(text=["начать подбор объединений", "начать подбор", "подбор", "показать еще рекомендации",
                      "показать ещё рекомендации", "показать еще", "показать ещё", "еще", "ещё"])
async def handler(message: Message):
    if message.from_id not in user_info:
        database.add_mailing(message.from_id)
        user_info[message.from_id] = {"club_index": 0,
                                      "recommendations": Recommender(15, message.from_id).get_recommended_stud_clubs()}

    if user_info[message.from_id]["club_index"] < len(user_info[message.from_id]["recommendations"]):
        templates = []
        for i in range(user_info[message.from_id]["club_index"],
                       user_info[message.from_id]["club_index"] + 5):
            keyboard_choice = Keyboard().add(
                OpenLink(user_info[message.from_id]["recommendations"][i][1], label="Паблик")).add(
                OpenLink(user_info[message.from_id]["recommendations"][i][2], label="Посмотреть на сайте")).get_json()
            templates.append(TemplateElement(
                description=user_info[message.from_id]["recommendations"][i][0][:80],
                photo_id=user_info[message.from_id]["recommendations"][i][3],
                buttons=keyboard_choice))
        user_info[message.from_id]["club_index"] += 5
        try:
            carousel = template_gen(*templates).decode()
        except:
            carousel = template_gen(*templates)

        if user_info[message.from_id]["club_index"] == 5:
            if len(user_info[message.from_id]["recommendations"]) == 15:
                first_message = "🤓 Искусственный интеллект подобрал для тебя эти студенческие объединения ТюмГУ 👇🏻"
            else:
                first_message = "🔮 У тебя закрытый профиль или нет подписок, " \
                                "поэтому вот случайные объединения 👇🏻"
        else:
            first_message = "Ещё пять объединений 👇🏻"
        await message.answer(
            message=first_message,
            template=carousel, random_id=0)

        if user_info[message.from_id]["club_index"] < len(user_info[message.from_id]["recommendations"]):
            # keyboard_choice = (
            #     Keyboard().add(Text("Что есть на UTMN FEST"),
            #                    color=KeyboardButtonColor.NEGATIVE
            #                    ).row()
            #         .add(Text("Показать ещё рекомендации"),
            #              color=KeyboardButtonColor.PRIMARY
            #              ).row()
            #         .add(Text("Показать все объединения"))
            #         .add(Text("Задать вопрос организаторам"))
            #         .get_json()
            # )
            keyboard_choice = (
                Keyboard().add(Text("Показать ещё рекомендации"),
                         color=KeyboardButtonColor.PRIMARY
                         ).row()
                    .add(Text("Показать все объединения"))
                    .add(Text("Задать вопрос организаторам"))
                    .get_json()
            )
            last_message = "Если ты хочешь увидеть ещё несколько объединений, " \
                           "нажми кнопку «Показать ещё рекомендации» или напиши «Ещё»."
        else:
            user_info.pop(message.from_id)
            # keyboard_choice = (
            #     Keyboard().add(Text("Что есть на UTMN FEST"),
            #                    color=KeyboardButtonColor.NEGATIVE
            #                    ).row()
            #     .add(Text("Начать подбор объединений"),
            #          color=KeyboardButtonColor.PRIMARY
            #          ).row()
            #     .add(Text("Показать все объединения"))
            #     .add(Text("Задать вопрос организаторам"))
            #     .get_json()
            # )
            # last_message = "На этом рекомендации закончились! 🙈\n\n" \
            #                "✅ Чтобы узнать, какие есть активности на UTMN FEST, " \
            #                "нажми на кнопку «Что есть на UTMN FEST» или отправь сообщение «FEST»\n" \
            #                "✅ Чтобы посмотреть все студенческие объединения ТюмГУ, " \
            #                "нажми на кнопку «Показать все объединения» или отправь сообщение «Все»\n" \
            #                "✅ Чтобы задать вопрос организаторам UTMN FEST, " \
            #                "нажми на кнопку «Задать вопрос организаторам» или напиши «Вопрос»"
            keyboard_choice = (
                Keyboard().add(Text("Начать подбор объединений"),
                         color=KeyboardButtonColor.PRIMARY
                         ).row()
                    .add(Text("Показать все объединения"))
                    .add(Text("Задать вопрос организаторам"))
                    .get_json()
            )
            last_message = "На этом рекомендации закончились! 🙈\n\n" \
                           "✅ Чтобы посмотреть все студенческие объединения ТюмГУ, " \
                           "нажми на кнопку «Показать все объединения» или отправь сообщение «Все»\n" \
                           "✅ Чтобы задать вопрос организаторам UTMN FEST, " \
                           "нажми на кнопку «Задать вопрос организаторам» или напиши «Вопрос»"
        await message.answer(
            message=last_message,
            keyboard=keyboard_choice, random_id=0)


@bot.on.message(text=["показать все объединения", "показать все", "все объединения", "все"])
async def handler(message: Message):
    themes = database.get_themes()
    if len(themes) == 0:
        await message.answer("😔 Категории студенческих объединений не найдены.")
    else:
        keyboards = [Keyboard(inline=True) for i in range(math.ceil(len(themes) / 5))]
        for i in range(len(themes)):
            theme = themes[i][1]
            if len(theme) > 40:
                theme = theme[0:37] + "..."
            keyboards[i // 5].row()
            keyboards[i // 5].add(Text(theme, {"cmd": themes[i][0]}), color=KeyboardButtonColor.SECONDARY)
        theme_message = "Выбери категорию 👇🏻"
        for i in range(len(keyboards)):
            await message.answer(
                message=theme_message,
                keyboard=keyboards[i].get_json(),
                random_id=0
            )
            theme_message = "⠀"


@bot.on.message(payload=[{"cmd": i} for i in range(10000)])
async def handler_theme(message: Message):
    payload = json.loads(message.payload)
    id = payload["cmd"]
    studclubs = database.get_by_theme_id(id)
    if len(studclubs) == 0:
        await message.answer("😔 Студенческих объединений в данной категории нет.")
    else:
        templates = [[] for i in range(math.ceil(len(studclubs) / 10))]
        for i in range(len(studclubs)):
            keyboard_choice = Keyboard().add(
                OpenLink(studclubs[i][2], label="Посмотреть на сайте")).get_json()
            templates[i // 10].append(TemplateElement(
                description=studclubs[i][0][:80],
                photo_id=studclubs[i][3],
                buttons=keyboard_choice
            ))
        theme_first = "Вот объединения выбранной категории"
        for i in range(len(templates)):
            try:
                carousel = template_gen(*templates[i]).decode()
            except:
                carousel = template_gen(*templates[i])
            if len(templates) > 1:
                theme = theme_first + f" ({i + 1}/{len(templates)}) 👇🏻"
            else:
                theme = theme_first + f" 👇🏻"
            await bot.callback.api.messages.send(user_id=message.from_id, message=theme,
                                                 template=carousel, random_id=0)
            theme_first = "Вот ещё объединения выбранной категории"


@bot.on.message(text=["задать вопрос организаторам", "вопрос"])
async def handler(message: Message):
    await message.answer(message="Отправь сообщение с вопросом, скоро на него ответит наш медиаменеджер 😉")


@bot.on.message(RegexRule("!send "), permission=[181438748, 388376198])
async def handler(message: Message):
    for _id in database.get_mailing_ids():
        try:
            await bot.api.messages.send(user_id=_id, message=message.text[6:], random_id=0)
        except:
            continue


# @bot.on.message(text=["карта фестиваля", "карта"]) #ready
# async def handler(message: Message):
#     await message.answer(
#         message="Специально для тебя — карта Фестиваля студенческих объединений UTMN FEST 👇🏻",
#         attachment="photo-185043129_457246239",
#         random_id=0)
#
#
# @bot.on.message(text=["расписание фестиваля", "расписание"]) #ready
# async def handler(message: Message):
#     await message.answer(
#         message="Расписание точек притяжения UTMN FEST: главной сцены, студенческого центра и лектория 👇🏻",
#         attachment="photo-185043129_457247406,photo-185043129_457247407,photo-185043129_457247412",
#         random_id=0)
#
#
# @bot.on.message(text=["студенческие объединения", "объединения"]) #ready
# async def handler(message: Message):
#     await message.answer(
#         message="Хочешь узнать больше про студенческие объединения, представленные на UTMN FEST? "
#                 "Читай наш лонгрид в 5 частях 👇🏻\n"
#                 "✅ https://vk.com/@utmn_fest-part-1-studencheskie-obedineniya\n"
#                 "✅ https://vk.com/@utmn_fest-part-2-studencheskie-obedineniya\n"
#                 "✅ https://vk.com/@utmn_fest-part-3-studencheskie-obedineniya\n"
#                 "✅ https://vk.com/@utmn_fest-part-4-studencheskie-obedineniya\n"
#                 "✅ https://vk.com/@utmn_fest-part-5-studencheskie-obedineniya",
#         attachment="photo-212611518_457239674",
#         random_id=0)
#
#
# @bot.on.message(text=["активности фестиваля", "активности"]) #ready
# async def handler(message: Message):
#     await message.answer(
#         message="Лови информацию о дополнительных активностях нашего фестиваля 👇🏻",
#         attachment="wall-185043129_1368",
#         random_id=0)
#
#
# @bot.on.message(text=["фудкорт"]) #ready
# async def handler(message: Message):
#     await message.answer(
#         message="Всех проголодавшихся ждём на фудкорте 👇🏻",
#         attachment="wall-185043129_1359",
#         random_id=0)
#
#
# @bot.on.message(text=["розыгрыш"]) #ready
# async def handler(message: Message):
#     await message.answer(
#         message="Как получить крутые подарки на UTMN FEST? Скорее смотри в посте 👇🏻",
#         attachment="wall-185043129_1354",
#         random_id=0)
#
#
# @bot.on.message(text=["трансляция фестиваля", "трансляция"]) #ready
# async def handler(message: Message):
#     await message.answer(
#         message="Подключайся к прямой трансляции UTMN FEST 2022 4 сентября с 15:30 👇🏻",
#         attachment="video-45388337_456239583",
#         random_id=0)
#
#
# @bot.on.message(text=["подписаться на молодёжку"]) #ready
# async def handler(message: Message):
#     await message.answer(
#         message="Чтобы узнавать первым про мероприятия, конкурсы и возможности, подписывайся на рассылку "
#                 "@molpolutmn (Молодёжки ТюмГУ): https://vk.cc/cgf0sa 😉",
#         random_id=0)


@bot.on.message(text=["что есть на utmn fest", "fest"])
async def handler(message: Message):
    # keyboard_choice = (
    #     Keyboard(inline=True).add(Text("Карта")).add(Text("Расписание")).row()
    #         .add(Text("Объединения")).add(Text("Активности")).row()
    #         .add(Text("Фудкорт")).add(Text("Розыгрыш")).row()
    #         .add(Text("Трансляция")).add(Text("Подписаться на Молодёжку"))
    #         .get_json()
    # )
    keyboard_choice = (
        Keyboard().add(Text("Начать подбор объединений"),
                       color=KeyboardButtonColor.PRIMARY
                       ).row()
            .add(Text("Показать все объединения"))
            .add(Text("Задать вопрос организаторам"))
            .get_json()
    )
    await message.answer(
        message="UTMN FEST завершился. До встречи в следующем году!\n\n"
                "Чтобы узнавать первым про мероприятия, конкурсы и возможности, подписывайся на рассылку "
                "@molpolutmn (Молодёжки ТюмГУ): https://vk.cc/cgf0sa 😉",
        keyboard=keyboard_choice, random_id=0)


if __name__ == "__main__":
    bot.run_forever()
